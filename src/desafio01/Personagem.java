package desafio01;

public abstract  class Personagem {
    //Atritubos
    private String name;
    protected int vida;
    private int level;
    private int dinheiro;
    private int atack;
    protected int defesa;

    public Personagem(String name, int vida, int level, int dinheiro, int atack, int defesa) {
        this.name = name;
        this.vida = vida;
        this.level = level;
        this.dinheiro = dinheiro;
        this.atack = atack;
        this.defesa = defesa;
    }

    public void atacar(Personagem oponente) {
        oponente.vida -= atack;
    }

    public void defender(Personagem defensor) {
        defensor.vida += defesa;
    }

    public void caminnhar() {
    }
    
    public void ataqueEspecial(Personagem oponente) {
        oponente.vida -= atack + 5;
    }



    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public int getLevel() {
        return this.level;
    }

    public  void setLevel(int level) {
        this.level = level;
    }

    public int getDinheiro() {
        return dinheiro;
    }

    public void setDinheiro(int dinheiro) {
        this.dinheiro = dinheiro;
    }

    public int getAtack() {
        return this.atack;
    }

    public void setAtack(int atack) {
        this.atack = atack;
    }

    public int getDefesa() {
        return this.defesa;
    }

    public void setDefesa(int defesa) {
        this.defesa = defesa;
    }


    

    @Override
    public String toString() {
        return "Personagem{" +
                "name='" + name + '\'' +
                ", vida=" + vida +
                ", level=" + level +
                ", dinheiro=" + dinheiro +
                ", atack=" + atack +
                ", defesa=" + defesa +
                '}';
    }


}














