package desafio01;


import java.util.ArrayList;
import java.util.List;

public class Guerreiro extends Personagem  {

    private static int bravura;
    private static List<String> inventario;

    public Guerreiro(String name, int vida, int level, int dinheiro, int atack, int defesa) {
        super(name, vida, level, dinheiro, atack, defesa);
        bravura = 10;
        inventario= new ArrayList<>();
    }

    public void ataqueEspecial(Personagem oponente) {
        oponente.vida -= getAtack() * bravura;

    }
    public void atacar(Personagem oponente){
        oponente.vida -= getAtack() - 20;

    }

    public void  defender(Personagem defensor){
        defensor.vida += getAtack() + defesa;



    }


    public int getBravura() {
        return bravura;
    }


    public void addInventario(String item){
        inventario.add(item);

    }


    public List<String> getInventario(){
        return inventario;
    }
    public  void removeInventario(String item){
        inventario.remove(item);
    }
    public boolean  itemExitente(String item){
        return inventario.contains(item);

    }


}

