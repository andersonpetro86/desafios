package desafio01;

public  class Necromante extends Personagem{

    private static int inteligencia;   //especialidade

    public Necromante(String name, int vida, int level, int dinheiro, int atack, int defesa) {
        super(name, vida, level, dinheiro, atack, defesa);
        inteligencia = 10;
    }

    @Override
    public void ataqueEspecial(Personagem oponente) {
        oponente.vida -= getAtack() * inteligencia;
    }

    public int getInteligencia() {
        return inteligencia;
    }
    public void defender(Personagem defensor){
        defensor.vida += getAtack() + defesa;

    }

    public void atacar(Personagem oponente){
        oponente.vida -= getAtack() - 15;

    }
}