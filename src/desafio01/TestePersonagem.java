package desafio01;


import java.util.Scanner;

public class TestePersonagem {



    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);


       Guerreiro guerreiro = new Guerreiro("John", 400, 10, 100, 20, 10);
       var escudo = "escudo";
       var espada = "espada";

       guerreiro.addInventario("escudo");
       guerreiro.addInventario("espada");

       System.out.println(" Inventario atual");
       guerreiro.getInventario().forEach(value -> System.out.println(" items -> ".concat(value)));


       System.out.println("Verifica se o item esteja no inventario ".concat(espada));
       System.out.println(guerreiro.itemExitente(espada));

       System.out.println("Verifica se o item esteja no inventario ".concat(escudo));
       System.out.println(guerreiro.itemExitente(escudo));
       System.out.println(guerreiro.toString());


       System.out.println(" Removendo o item ".concat(escudo));

       guerreiro.removeInventario(escudo);
       System.out.println(guerreiro);
       System.out.println(" Inventario atual");
       System.out.println();
       System.out.println("O GUERREIRO ESTÁ PRONTO PARA A BATALHA!");
       System.out.println(guerreiro.toString());

       System.out.println();

       System.out.println();
       System.out.println("O GUERREIRO ESTÁ PRONTO PARA A BATALHA!");
       System.out.println(guerreiro.toString());
       System.out.println("Característica especial: Bravura = " + guerreiro.getBravura());
       System.out.println();


       Necromante necromante = new Necromante("Norman", 400, 5, 100, 15, 15);

       System.out.println();
       System.out.println("O NECROMANTE ESTÁ PRONTO PARA A BATALHA!");
       System.out.println(necromante.toString());
       System.out.println("Característica especial: Inteligencia = " + necromante.getInteligencia());
       System.out.println();

       System.out.println("INICIAR BATALHA!");
       System.out.println("Guerreiro tem ==> " + "Vida " + guerreiro.getVida());
       System.out.println("Necromante tem ==> " + "Vida " + necromante.getVida());

       guerreiro.caminnhar();
       guerreiro.atacar(necromante);
       necromante.defender(necromante);
       System.out.println();
       guerreiro.ataqueEspecial(necromante);
       necromante.defender(guerreiro);

       System.out.println("-------------");

       necromante.caminnhar();
       necromante.atacar(guerreiro);
       guerreiro.defender(guerreiro);
       necromante.ataqueEspecial(guerreiro);
       guerreiro.defender(guerreiro);

       System.out.println();
       System.out.println("Situação da Batalha...");
       System.out.println(guerreiro.toString());
       System.out.println(necromante.toString());

       System.out.println("Outra BATALHA!");
       System.out.println("Guerreiro tem ==> " + "Vida " + guerreiro.getVida());
       System.out.println("Necromante tem ==> " + "Vida " + necromante.getVida());

       guerreiro.caminnhar();
       guerreiro.atacar(necromante);
       necromante.defender(necromante);
       System.out.println();
       guerreiro.ataqueEspecial(necromante);
       necromante.defender(guerreiro);

       necromante.caminnhar();
       necromante.atacar(guerreiro);
       guerreiro.defender(guerreiro);
       necromante.ataqueEspecial(guerreiro);
       guerreiro.defender(guerreiro);

       System.out.println();
       System.out.println("Situação da Batalha...");
       System.out.println(guerreiro);
       System.out.println(necromante);



       sc.close();

    }
}
